﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contains all views related to the app.
public class View : Djemmer
{
	private static View view;
	public static View instance 
	{
		get 
		{
			if (view == null)
				view = GameObject.FindObjectOfType<View>();

			return view;
		}
	}

	// Reference to the ball
	public BallView ball;
	public CounterView counter;
}