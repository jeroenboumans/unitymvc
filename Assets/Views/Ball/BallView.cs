﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Describes the Ball view and its features.
public class BallView : Djemmer
{
	// Only this is necessary. Physics is doing the rest of work.
	// Callback called upon collision.
	void OnCollisionEnter2D(Collision2D collision) 
	{ 
		app.controller.ballController.OnBallGroundHit ();
	}
}