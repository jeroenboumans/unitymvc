﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterController : Djemmer {

	private static CounterController counterController;
	public static CounterController instance
	{
		get 
		{ 
			if (counterController == null)
				counterController = GameObject.FindObjectOfType<CounterController>();

			return counterController;
		}
	}

	public void UpdateCounter()
	{
		int bounces = app.model.ballModel.bounces;
		app.model.counterModel.score = bounces;
		app.view.counter.UpdateScore ( bounces.ToString() );
	}
}
