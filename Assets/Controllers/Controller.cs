﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : Djemmer 
{
	private static Controller controller;
	public static Controller instance 
	{
		get 
		{ 
			if (controller == null)
				controller = GameObject.FindObjectOfType<Controller>();

			return controller;
		}
	}


	// Register controllers here

	public BallController ballController 
	{
		get{ return BallController.instance; }
	}

	public CounterController counterController 
	{
		get{ return CounterController.instance; }
	}

	public GameController gameController 
	{
		get{ return GameController.instance; }
	}
}
