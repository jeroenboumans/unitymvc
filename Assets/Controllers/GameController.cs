﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Djemmer {

	private static GameController gameController;
	public static GameController instance
	{
		get 
		{ 
			if (gameController == null)
				gameController = GameObject.FindObjectOfType<GameController>();

			return gameController;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnGameComplete()
	{
		Debug.Log("Victory!!");
	}
}
