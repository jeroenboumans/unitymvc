﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : Djemmer 
{

	private static BallController ballController;
	public static BallController instance
	{
		get 
		{ 
			if (ballController == null)
				ballController = GameObject.FindObjectOfType<BallController>();

			return ballController;
		}
	}

	public void OnBallGroundHit()
	{
		app.model.ballModel.bounces++;

		Debug.Log("Bounce "+app.model.ballModel.bounces);

		if(app.model.ballModel.bounces >= app.model.gameModel.winCondition)
		{
			app.view.ball.enabled = false;
			app.view.ball.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			app.view.ball.GetComponent<Rigidbody2D>().isKinematic=true; // stops the ball
			app.model.gameModel.OnGameComplete();
		}	
	}
}
