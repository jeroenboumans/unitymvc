﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contains all data related to the app.

public class Model : Djemmer
{
	
	private static Model model;
	public static Model instance 
	{
		get 
		{ 
			if (model == null)
				model = GameObject.FindObjectOfType<Model>();

			return model;
		}
	}


	// Register controllers here

	public BallModel ballModel 
	{
		get{ return BallModel.instance; }
	}

	public GameModel gameModel 
	{
		get{ return GameModel.instance; }
	}

	public CounterModel counterModel 
	{
		get{ return CounterModel.instance; }
	}
}
