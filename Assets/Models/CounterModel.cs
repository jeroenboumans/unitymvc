﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterModel : Djemmer {

	private static CounterModel counterModel;
	public static CounterModel instance
	{
		get 
		{ 
			if (counterModel == null)
				counterModel = GameObject.FindObjectOfType<CounterModel>();

			return counterModel;
		}
	}

	public int score;
}
