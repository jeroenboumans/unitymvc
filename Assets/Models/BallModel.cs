﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallModel : Djemmer {

	private static BallModel ballModel;
	public static BallModel instance
	{
		get 
		{ 
			if (ballModel == null)
				ballModel = GameObject.FindObjectOfType<BallModel>();

			return ballModel;
		}
	}


	// Data
	private int _bounces;
	public int bounces {
		get{
			return _bounces;
		}
		set {
			_bounces = value;
			app.controller.counterController.UpdateCounter ();
		}
	}




	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
