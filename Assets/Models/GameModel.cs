﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel : Djemmer {

	private static GameModel gameModel;
	public static GameModel instance
	{
		get 
		{ 
			if (gameModel == null)
				gameModel = GameObject.FindObjectOfType<GameModel>();

			return gameModel;
		}
	}

	public int winCondition;
	public int Level;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnGameComplete()
	{
		Debug.Log("Victory!!");
	}
}
