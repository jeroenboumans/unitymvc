﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Djemmer : MonoBehaviour
{
	// Gives access to the application and all instances.
	private Application _app;
	public Application app 
	{ 
		get 
		{ 
			if (_app == null)
				_app = GameObject.FindObjectOfType<Application>();

			return _app;
		}
	}
}

// Base class for all MVC base-elements in this application.
public class Application : MonoBehaviour 
{

	public Model model 
	{
		get{ return Model.instance; }
	}

	public View view 
	{
		get{ return View.instance; }
	}

	public Controller controller 
	{
		get{ return Controller.instance; }
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}